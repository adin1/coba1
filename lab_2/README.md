# What should you learn from codes & documentation in this sub-directory?

* The implementation of View: the presentation layer to the user, what you see on the browser
* The implementation of Controller: intermediary tier between model and view
* How Django Webserver Works ![How Django Webserver Works](docs/how-django-webserver-works.png)
* How Django Framework Works with Templating ![How Django Framework Works with Templating](docs/how-django-framework-works.png)
* How Django Framework Works with Templating & Models ![How Django Framework Works with Templating & Models](docs/how-django-framework-with-models-works.png)

## Checklist Lab 2

1. Create your _Landing Page_
    1. [ ] Change the content of variabel `landing_page_content` to be your desired _Landing Page_ (Min 30 characters)
    2. [ ] Configure your urls.py file `lab_2/urls.py` so that the _Landing Page_ can be accessed on URL `<HEROKU_APP_HOST>/lab-2/`
        > example : djangoppw.herokuapp.com/lab-2/
2. Create new Django Apps named *lab_2_addon* and configuure your `views.py` in that new Django apps
    1. [ ] Create new `app`  (Hint: Execute  `python manage.py help` to know manage.py available parameters)
    2. [ ] Create/modify your `views.py` on your new `app` using the code below, and make some necessary modifications
    
```python
    from django.shortcuts import render
    from lab_1.views import mhs_name, birth_date
    #Create a list of biodata that you wanna show on webpage:
    #[{'subject' : 'Name', 'value' : 'Igor'},{{'subject' : 'Birth Date', 'value' : '11 August 1970'},{{'subject' : 'Sex', 'value' : 'Male'}
    #TODO Implement
    bio_dict = [{'subject' : 'Name', 'value' : mhs_name},\
    {'subject' : 'Birth Date', 'value' : birth_date.strftime('%d %B %Y')},\
    {'subject' : 'Sex', 'value' : ''}]
    
    def index(request):
        response = {}
        return render(request, 'description_lab2addon.html', response)
```
4. Create a _folder templates_ in `apps lab_2_addon` to keep your HTML/template _file_ HTML for `apps lab_2_addon`:
    1. [ ] Create _folder templates_ in `apps lab_2_addon`
    2. [ ] Copy _file_ `lab_2/templates/description_lab2addon.html` into _folder templates_ in `apps lab_2_addon`
5. Configure your routing so that `description_lab2addon.html` can be accessed properly
    1. [ ] Modify your `urls.py` in _folder_ `lab_2_addon` and `praktikum` for accessing your web application on URL `<HEROKU_APP_HOST>/lab-2-addon/`
    2. [ ] Modify your _section_ INSTALLED_APPS so that `apps lab_2_addon` will be registered as an active _Django Apps_ Tanpa melakukan langkah ini
    maka halaman `description_lab2addon.html` tidak bisa ditampilkan melalui URL yang sudah dibuat di `urls.py`)
    3. [ ] Tampilkan halaman _Landing Page_ jika ada _request_ yang datang pada Root URL _website_ kalian
        >Ketika mengakses `<HEROKU_APP_HOST>/` maka _Landing Page_ akan tampil (Hint: Gunakan RedirectView)
    4. [ ] Isilah _file_ `lab_2_addon/tests.py` dengan cara memindahkan `class Lab2AddonUnitTest` beserta semua
    _Test Case_ yang ada lalu aktifkan _Test Case_ tersebut dengan menghilangkan `skip` di atas setiap _Test Case_.
    Import semua `library`, `function` atau `variabel` yang dibutuhkan agar _Test Case_ bisa dijalankan
        
6. Pastikan kalian memiliki _Code Coverage_ yang baik
    1. [ ] Jika kalian belum melakukan konfigurasi untuk menampilkan _Code Coverage_ di Gitlab maka lihat langkah `Show Code Coverage in Gitlab`
    di [README.md](https://gitlab.com/PPW-2017/Draft-Lab/blob/master/README.md)
    2. [ ] Pastikan _Code Coverage_ kalian 100%


# References

* Class Material "02. Intro to Framework, Server Side Programming with Django", Slide 1-31
* https://www.djangoproject.com/
* https://docs.djangoproject.com/en/1.11/
* https://docs.djangoproject.com/en/1.11/topics/http/urls/
